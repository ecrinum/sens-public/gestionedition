# Notes pour les coordonnatrices 

## Priorité 

Dossier Textures : Informations à compléter dans les articles : 

url de l'article et identifiant à contrôler

Dans yaml : mots-clefs contrôlés (a copier dans le mode édition de la note)

controlledKeywords:
  - idRameau: FRBNF120218114
    label: Arts et Lettres
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb12021811z
  - idRameau: FRBNF133328055
    label: Monde numérique
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb133328054
  - idRameau: FRBNF13318593
    label: Édition, presse et médias
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb13318593f
 
Les informations sur la revue : 

Sens public
*pas de majuscule à public* 

Directeur : 
Wormser Gérard : 0000-0002-6651-1650

ISSN : 2104-3272

Directeurs du dossier : **toujours dans le même ordre**
Anne Chassagnol : 0000-0001-6154-7040
Gwen Le Cor : 0000-0001-8908-9934

Titre du dossier : Textures : l'objet livre du papier au numérique
id : SP1486

- [x] 1486 : *modèle que vous pouvez aller consulter* 
- [x] 1487
- [x] 1488
- [x] 1489 
    - [ ] manque logo et credit + orcid
- [x] 1490
- [x] 1491 
- [x] 1492
- [x] 1493 
- [x] 1494
- [x] 1495
- [x] 1496
- [x] 1497 
- [x] 1498 
- [x] 1499
    - [ ] attente retours édition + images + logo + credit logo 
- [x] 1501
- [x] 1502 
- [x] 1504 
- [x] 1505


## Dossier images composites
Identifiants des images artonID
Habituellement la légende doit être dans le yaml, ajoutée dans le raw yaml sous la forme suivante 
logocredits:'legende'

- 1573 : Pas de légende pour l'image logo? Sinon ok
- 1574 : Pas de légende pour l'image logo? Une partie importante des notes en bas de page n'existent que pour indiquer une référence (ex, note 25). Ces notes doivent être insérées en corps de texte comme les autres références, sauf bien sûr, quand il s'agit d'apporter une précision à la référence, comme dans le cas de la note 15. 
- 1575 : Dans les sections où cela semble être des témoignages, il serait peut-être judicieux de mettre des guillemets pour que cela soit plus clair ? Mais c'est vous qui voyez là-dessus. Ça peut rester comme ça. De la même façon, je me questionne sur les sous-titres très rapprochés les uns des autres au début du texte, mais peut-être est-ce l'effet recherché, après tout le texte est classé comme lecture donc encore une fois à vous de voir.
- 1576 : Pas de légende pour l'image logo? Sinon ok.
- 1577 : Pas de légende pour l'image logo? Sinon ok.
- 1578 : Pas de légende pour l'image logo? Les appels de note (surtout ici pour les traductions) doivent être à l'intérieur des guillemets, avant toute ponctuation. Sinon ok.
- 1579 : Pas de légende pour l'image logo? Sinon ok.
- 1580 : Pas de légende pour l'image logo? Comme pour le 1574, certaines notes de bas de page ne servent qu'à indiquer des références, il faut les inclure dans le corps du texte comme les autres références. La note 28 peut rester comme ell est inscrite actuellement, mais pas la 29, par exemple.
- 1581 : Pas de légende pour l'image logo? Aussi, je l'ai modifié lorsque cela se produisait, mais il est important de bien mettre la référence accolée à la citation, et pas au nom de l'auteur. Par exemple,
Comme le dit Deleuze (-@deleuze_1980) : « xyz ». 
devrait être écrit
Comme le dit Deleuze : « xyz » (-@deleuze_1980). Sinon ok.
- 1582 : Il semble y avoir des commentaires de révision dans le texte. Ça ne dérange pas, ils n'apparaissent pas, mais peut-être que vous voulez les regarder à nouveau. Certaines ne semblent pas être résolues, comme celle à propos du nom LOLA ou Lola (peu avant la note 18).
-J'en ai corrigé les occurrences, mais habituellement il faut indiquer la référence après le point final des citations en retrait, pas à l'extérieur. 
Il semble y avoir un problème avec la référence [@gaudreault_cinema_2008] qui ne s'affiche pas dans le preview car elle n'est pas dans le bib d'après ce que je comprends. Je n'arrive pas à produire le pdf de cet article, je devrai réessayer une fois que le bib sera complet. Je n'ai pas encore réussi à trouver d'où vient le problème, si ça ne vient pas de là, je demanderai à Margot. Peut-être qu'il serait judicieux d'enlever les commentaires (mais je ne pense pas que le problème vienne de là), ou alors de modifier les légendes des images pour en mettre les descriptions plus longues en note de bas de page plutôt qu'en légende. 
- 1613 : Ok.
- 1614 : Ok
- 1615 : Ok
- 1616 : Les notes 11, 13 18, 22 devraient être incluses dans le corps du texte comme références. La clef [-@bianchini_ta_2000] ne fonctionne pas. 

Attention particulière lors de l'export : 
- 1574 (resize)
- 1576
- 1582 pas de table des matières
- 1614 mots clefs autre page

Export final
- 1573 OK
- 1574 : [@sacco_soleil_2020] la référence n'existe pas
- 1575 : OK
- 1576 : OK
- 1577 : OK
- 1578 : OK
- 1579 : OK
- 1580 : OK
- 1581 : OK
- 1582 : OK
- 1613 : OK
- 1614 : OK
- 
## Accès 

Mail : https://gator3066.hostgator.com:2096
mp : (R$MBkp#wM@c

Imgur : 
https://senspublic.imgur.com
mp : GérardForEver1

## Commandes pour image 

convert *.jpg *.png 

convert *.jpg -resize 500x1050 *.png

## Commande pour convertir latex en pdf 
xelatex *.tex *.pdf

## Retouches LaTex
Si saut de page sur la première page : 
Jouer avec les espaces entre les éléments : 
\vspace*{xcm}
vspace = vertical space
Cet espace vertical peut aussi être négatif (par exemple pour éliiminer des espaces trop grands en haut d'une page alors que les keywords débordent sur une nouvelle page).

## Du côté QC
### Textures (juin) 
- 1486 : attente retour directrice
- 1499 : attente retour auteure
- 1489 : attente retour auteure
- 1504 : attente retour BAT 

## Notes sur l'export
- Les images logo doivent être en png ou en jpg (pas .jpeg ou .JPG).

### Crihn (Juillet)

### Route 

## Du côté FR

### Dossier Féminisme - 1583

Vérifications : 
- la preview fonctionne 
- l'export pdf fonctionne 
- les mots clefs catégories correspondent à des mots-clefs éditeur dans le site et ont les bons identifiants (raw yaml)
- la légende est dans le yaml sinon le signaler 
- contrôler l'édition (insertion biblio, niveaux de titres, etc.): **Si la reprise de l'édition prend plus de 10 min : faites une liste des choses à faire / rééediter et on renvoit**
- si images : en png, si jpg on renvoit
- vérifier que les articles ont les mêmes informations concernant le dossier (nom directeurs dans le même ordre, même titre, même identifiant du dossier)


- CQ : *lister les problèmes des articles à la suite*

Les images logo sont en commentaires dans les articles. J'ai ajouté les légendes dans le yaml.
    
    point général : si bibtex dans note, note en fin de page pas in texte.
    
- [x] 1583 - valider l'annotation hypothes.is 
    - [x] 1584 - il y a une note dans le sous-titre qui ne fct pas dans l'export sens public - à changer - un des titres est long pour l'export, est-ce que l'espace est normal? - image 2 très grande, une page complète.
    - [x] 1585 - bibliographie : manque queques dates : Ancery, Pierre, Daniel-Lesueur, Rouyer Muriel et Séverine.
    - [x] 1586 - 3 premières images grandes, prend une page dans l'export.
    - [x] 1587 - Bibliographie : manque date : Fajardo, Alicia. s. d. « Monstruosa nube… Los envenedadores de Seveso ». Vindicación Feminista, nᵒ 7. 
    - [x] 1588 : bibliographie manque date s. d. : Castellina, Luciana et note 36 dans le preview
    - [x] 1589
    - [x] 1590 : Bibliograpgie : Servén Díez, Carmen s.f.? 
    - [x] 1591 - sous-titre trop long export.
    

### Dossier Pudeur - 1541
CQ + insert légende des images logo dans yaml avec : 
logocredits: "legende"

Eugénie : 1542 --> 1550
Margot : 1541
Christine : 1551 --> 1559

Vérifications : 

- la preview fonctionne 
- l'export pdf fonctionne 
- les mots clefs catégories correspondent à des mots-clefs éditeur dans le site et ont les bons identifiants (raw yaml)

- ajouter la legende dans le raw yaml : <
logocredits: "legende"
- contrôler l'édition (insertion biblio, niveaux de titres, etc.): **Si la reprise de l'édition prend plus de 10 min : faites une liste des choses à faire / rééediter et on renvoit**

- si images : en png, si jpg on renvoit
- vérifier que les articles ont les mêmes informations concernant le dossier (nom directeurs dans le même ordre, même titre, même identifiant du dossier)

### Retours sur les articles 
*si des CQ prennent trop de temps, vous pouvez lister ici les éléments à revoir par articles.* 
1543 : La citation en exergue au début de l'article  n'apparaît pas en exergue dans le pdf (mais apparaît correctement dans la preview).
1544 : Au onzième paragraphe, « Ce monotype (1989) » : la date n'est pas liée à une référence.
1545 : de nombreux commentaires (n'apparaissant pas dans les preview) restent dans le md sur stylo. Faut-il les traiter ou les retirer?
1554 = pas de catégorie, dans le texte il y a : Isabelle {barré en I} à valider, manque la note [^61] à la fin du texte. Donc soit ajouter la note de base de page ou effacer [^61] dans le texte.
1555 = Manque la date de « Mauve le vierge » dans la bibliopgraphie.
1559 = pas de catégorie, sous-titre long - valider la mise en page


## Mails type 

[https://demo.hedgedoc.org/hSFYpTa9Rv2DVMunqdY5Iw#](https://demo.hedgedoc.org/hSFYpTa9Rv2DVMunqdY5Iw#)

## Retours sur Stylo

-Voir s'il est possible de mettre des exposants dans les titres des articles avec des templates (titre en  yaml, balisage md).

### développement
-Le « close » des volets de métadonnées et des informations se trouve en haut et on ne peut pas le fermer en milieu de volet
-Lorsque l'on crée une version « save », le curseur n'est pas automatiquement dans la boîte.
-Le bouton « close » compétitionne avec l'icône de copier coller des références bibliographiques. Mettre l'icône à gauche?  ou remettre la référence entière cliquable?
-L'outil de recherche pourrait être plus rapide à trouver sur le site de SP, ce n'est pas optimal.
-Options Stylo en français plutôt qu'en anglais?
-Dans l'éditeur de métadonnées, il y a une coquille : « ajouter une mot-clé contrôlé »
-lorsqu'on partage des articles d'un compte à un autre, nous ne pouvons plus écrire le courriel ou le nom de l'utilisateur et cliquer sur « share ». On doit trouver le courriel ou nom dans la liste.

## Notes Christine

### Publier un article sur le site [sens-public](https://sens-public.org/) : 

**Étape pour xml érudit :**

- Dans le repos du Gitlab
- Allez dans le répertoire idsp21.csv et compléter les information de l'article
- Mettre varia si l'article n'est pas dans un dossier et mettre l'orseq global de tous les articles.
- Si l'article est dans un dossier, mettre le id du dossier et l'orseq de l'article du dossier (pas le global). Lire le sommaire pour savoir l'ordre des articles ou demander au directeur.trice.
- Info à indiquer : type, id, ordseq
    - ESSAI, 58, 1-99
    - CHRONIQUE, 114, 100-199
    - ENTRETIEN, 113, 200-299
    - LECTURE, 76, 300-399
    - CREATION, 60, 400-499
- Mettre à jour le sommaire xml. (ajouter cette section au note)

1. **Créer le PDF de l'article - sens-public**

- À partir de stylo, créer une version majeure. Cliquer sur cette version et prendre la clé de version. 
- Copier la clé de version (se trouve à la fin de hyperlien de la version de l'article) dans [stylo-export](http://stylo-export.ecrituresnumeriques.ca/) et indiquer le ID de l'artcile (ex. SP1609)
- Choisir comme processor : xelatex
- Sélectionner table des matières seulement s'il y en a une.
- Appuyer sur submit
- Contrôler le PDF : page titre, table des matière, images, bibliographie, etc.

2. **Mettre l'article sur le repos**

- Si PDF est validé, télécharger le fichier zip de [stylo-export](http://stylo-export.ecrituresnumeriques.ca/).
- Mettre les images de l'article dans le dossier média du zip. Toutes les images doivent être en png. Nommer les images toujours par le ID de l'article et le numéro. ex. SP1402-img1.png
- Mettre l'image logo et la nommer : artonid.png ou jpg. L'image logo peut être en png ou jpg. Exemple : arton1487.png attention, on ne met pas le SP devant le id et on doit toujours mettre l'extension en minuscule.
- À partir du terminal, sauvegarder dans le [repos](https://framagit.org/laconis/SP-articles) en local dans le répertoire de l'article (nommé ID de l'article).
- Différentes commandes terminal :
    - cd edition2021
    - git pull
    - git add (mettre le numéro de l'article sx. SP1523)
    - git commit -m ""
    - git push 

3. **Publier l'article sur le site de sens-public**
- À partir du teminal, aller dans le repos, sphub et faire une mise à jour de tous les articles. Entrer la commande : $ bash update_article.....
- Cette action actualisera les articles déjà publiés, ne fait pas une importation des nouveaux dans le repos.
- Une fois la mise à jour effectuée, on doit absolument publier le sommaire en premier. Toujours à partir du terminal entrer la commande : .....
- À partir de l'interface graphique du site (Django), valider que le sommaire est bien en place et le publier.
- Valider que le sommaire est bien publié et conforme sur le site web Sens public avant de passer à l'étape suivante. 
- À partir du terminal, importer un par un les articles, dans le bon ordre, au dossier en entrant la commande :....
- Retourner sur l'interface graphique et valider que tous les articles sont présents et les publiés. 
- Les mettre dans le bon ordre à partir de l'interface graphique
- Faire un contrôle de qualité sur le site web de Sens public (dossier présent, tous les articles sont présents, images logo, images, liens, etc.)
- À partir du terminal refaire une mise à jour du repos par précaution. Entrer dans la commande : .... 
- À partir de l'interface graphique (Django) aller mettre le dossier en page de garde et valider que le tout est conforme sur le site.
- Finalement, informer les directeurs (dans le cas d'un dossier) ou auteur de la publication.

### Mots-clés

Dans stylo, il y a deux types de mots-clés:
- Mots-clés auteurs : Mots-clés choisis par l'auteur, on demande de se limiter à 6 mots clés
- Mots-clés contrôlés : Mots-clés déjà existants sur le site de sens public

Important, lors de la CQ, on doit valider que les mots-clés existent bel et bien sur le site de sens public.

- À partir de l'outil stylo, dans les métadonnées, dans l'onglet yaml, trouver les mots-clés contrôlés. Voici un exemple :
controlledKeywords:
  - idRameau: FRBNF120218114
    label: Arts et Lettres
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb12021811z
  - idRameau: FRBNF133328055
    label: Monde numérique
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb133328054
  - idRameau: FRBNF13318593
    label: Édition, presse et médias
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb13318593f

- À partir du site de sens public, sélectionner Index (en haut à gauche) et par la suite mots-clés.
- Trouver le premier mot-clé contrôlé du yaml dans la liste.
- Valider qu'il est bien présent et qu'il s'agit bel et bien d'un mot-clé contrôlé soit nommé Mot-clés — FR Éditeur
- En sélectionnant rameau, valider que le ID et le URI sont les mêmes que dans le yaml.
- Refaire les mêmes étapes pour chacun des mots-clés contrôlés du yaml.

Attention, le [site sens-public](https://sens-public.org/keywords/) inclus les mots-clés auteurs et éditeurs (contrôlés). Pour l'instant, il n'est pas possible de filtrer les mots-clés par type. 

### Autres notes

- Jusqu'à nouvel ordre, toujours indiquer cette note en début des articles dans stylo (voir mode édition) afin de s'assurer que la biographie soit complète.

---
nocite: '@*'

---

- Ne pas mettre d'exposant ni d'appel de note dans le titre ni dans les sous-tires

- L'image logo peut être en format jpg ou png. L'extension doit toujours être en minuscule. Toujours nommé l'image logo : arton.id.jpg ou png. ex. arton1502.png
- Le titre du dossier doit être le même que le titre de l'article sommaire.
- [Site web](https://girliemac.com/blog/2017/12/26/git-purr/) qui explique le principe du git 

## Notes pour tenter d'améliorer la légitimité des courriels (pour ne pas qu'ils se retrouvent dans les spams)
- Penser à avoir un objet clair, et peut-être penser à intégrer l'adresse physique du labo dans les signatures?
- Voir avec notre hébergeur si on a suivi ces choses : https://clients.whc.ca/fr/knowledgebase/115/Comment-ameliorer-la-reputation-de-mes-envois-courriels-SPF-DKIM.html?fbclid=IwAR2d33XHxXTjKqjiUGUZ0ZK95_ihtTsA7KIIQXQceLJlRol7y-UVre4KHfM
- Faire des tests avec https://www.mail-tester.com/

## Plan - Documentation éditeurs (suggestion - Christine)

### Présentation Sens-Public
- Histoire
- Fonctionnement
- Mission & Vision
- Équipe et rôles
- Chaire de recherche
### Présentation et fonctionnement des différents outils nécessaires
- Hypothes.is
- Stylo
- Orcid
- Zotero
- Documentation

### Premiers pas avec Stylo
### Étapes à suivre
### Édition d'un article - Éléments à valider
- Faire le suivi des annotations hypothes.is
- Niveau de titres
- Espaces insécables
- Images
- Notes
- Bibliographie
- Retour de l'auteur via annotations et correction s'il y a lieu

### CQ - Éléments à valider
- Titres et sous-titres
- ID de l'article
- Lien de l'article
- etc.

### Validation version de l'auteur

### CQ pour BAT

### Exportation et publication



###### tags `Chaire` `SP` `coordination`