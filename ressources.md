# Liste des ressources pour Sens public

## Documentation
- [Documentation Stylo pour SP](https://ecrinum.gitpages.huma-num.fr/sens-public/sp-stylo-doc/)
- [Protocole editeur et Checklist avant publication](documentationchaine/protocolechaine.md) 
- [Le wiki de publication](https://framagit.org/laconis/SP-articles/-/wikis/home) - qu'il faut mettre à jour!
- [Le wiki technique du Django](https://framagit.org/ecrinum/sp_hub/-/wikis/home)
- [Tutoriel markdown pandoc](https://gitlab.huma-num.fr/ecrinum/manuels/tutoriel-markdown-pandoc)
- [Documentation stylo](https://github.com/EcrituresNumeriques/stylo/tree/master/docs) - on travaille sr gh-pages qui est publié directement sur stylo-doc.ecrituresnumeriques.ca
- [La vieille chaîne de publication](https://github.com/EcrituresNumeriques/chaineEditorialeSP)

## Répertoires articles

- [Archives](https://framagit.org/laconis/SP-archives)
- [Articles](https://framagit.org/laconis/SP-articles/)
- [spall](https://gitlab.huma-num.fr/ecrinum/sens-public/sp-all) **iC'est le repos de référence à partir du 14 février 2024*

## Technique

- [process](https://framagit.org/stylo-editeur/process) c'est le repos qui gère l'export stylo à l'adresse stylo-export.ecrituresnumeriques.ca - les templates sont des submodules git qui renvoient au repos template-sp: attention, on travaille sur la branch book
- [templates](https://framagit.org/stylo-editeur/templates-sp) - ce sont les templates spécifiques POUR Sens public - Je pense que ce dossier est obsolète et que désormais les templates sont sur export-stylo
- [le keywords](https://framagit.org/marviro/spkeywords) - très important: mots-clés alignés, utilisés par le django
- [le script de conversion spip2erudit](https://github.com/EcrituresNumeriques/spip2erudit)

- [sp_hub](https://framagit.org/ecrinum/sp_hub/) c'est le repos qui gère la plateforme de sens-public.


## Édition

- Logos?
- Lettres type?
  - pour directeurs de dossier
  - pour auteur
  - pour évaluateur
- Présentation site
- protocole de soumission

## Liste complète repos

- [Archives](https://framagit.org/laconis/SP-archives)
- [Articles](https://framagit.org/laconis/SP-articles/)
- [spall](https://gitlab.huma-num.fr/ecrinum/sens-public/sp-all) **iC'est le repos de référence à partir du 14 février 2024*
- [sp_hub](https://framagit.org/ecrinum/sp_hub/) 
- [La vieille chaîne de publication](https://github.com/EcrituresNumeriques/chaineEditorialeSP)
- [le keywords](https://framagit.org/marviro/spkeywords) - très important: mots-clés alignés, utilisés par le django
- [le script de conversion spip2erudit](https://github.com/EcrituresNumeriques/spip2erudit)
- [templates](https://framagit.org/stylo-editeur/templates-sp) - ce sont les templates spécifiques POUR Sens public - Je pense que ce dossier est obsolète et que désormais les templates sont sur export-stylo
- [gestionedition](https://gitlab.huma-num.fr/ecrinum/sens-public/gestionedition)
- [doc stylo](https://gitlab.huma-num.fr/ecrinum/sens-public/sp-stylo-doc)
- [export stylo]()
