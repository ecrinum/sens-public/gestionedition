#!/bin/bash

REPO1="SP-archives"
REPO2="SP-articles"
DST_DIR="sp-all"

if [ "$1" == "" ]; then
    echo "Please provide the path to the folder where the 'article' repos are available."
    exit -1
fi

BASEDIR="$1"
CURDIR=$(pwd)

clean_folder () {
    rm -rf "$DST_DIR"
    mkdir -p "$DST_DIR"
}

update_repos () {
    echo "+++ Update git repos..."
    cd "$REPO1"
    git pull
    cd "../$REPO2"
    git pull
    cd ..
}

copy_sp_archives () {
    # Copy everything but 2016 and 2017 folders

    echo "Copy all articles from $REPO1, except 2016 and 2017..."
    find "$REPO1/erudithtml" \
        -type d -name "SP*" \
        -not -path "*/2016/SP*" \
        -not -path "*/2017/SP*" \
        -print0 \
    | xargs -0 -t -I {} cp -rp {} "$DST_DIR"

    # Look for 2016 articles, with ID < 1192
    echo "Copy all articles from $REPO1, from 2016 with ID < 1192..."
    find "$REPO1/erudithtml" -type d -name "SP*" -path "*/2016/SP*" -print \
    | awk '{n=split($0, a, "/"); split(a[n], sp_id, "SP"); if (sp_id[2] < 1192) print $0}' \
    | xargs -t -I {} cp -rp {} "$DST_DIR"
}

copy_sp_articles () {
    echo "Copy articles from $REPO2..."
    find "$REPO2" \
        -type d -name "SP*" \
        -path "*/Edition*/SP*" \
        -print0 \
    | xargs -0 -I {} -t cp -rp {} "$DST_DIR"
}

echo "!!! Starting."
cd "$BASEDIR" || exit -1
# clean_folder # pas besoin de nettoyer, on incrémente
update_repos
copy_sp_archives
copy_sp_articles
cd "$CURDIR"
echo "!!! Done."
echo "--- All articles are now in ${BASEDIR}/${DST_DIR}"
                                                         
