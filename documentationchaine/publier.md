## Publier
### Protocole de soumission des articles

#### Pourquoi ce protocole ?

La revue *Sens public* s'engage dans la production de contenus scientifiques structurés et conformes aux formats standards libres. Nous sommes en effet convaincu·e·s que la connaissance ne doit pas être enfermée dans des formats propriétaires comme le format .docx.

*Sens public* a donc fait des choix techniques en ce sens, en créant l'éditeur de texte [Stylo](https://stylo.ecrituresnumeriques.ca/), qui produit du HTML à partir des formats [markdown](https://daringfireball.net/projects/markdown/), yaml et bibtex, [désormais devenus des standards pour l'édition savante](https://memoire.quaternum.net/). Stylo propose un environnement d'édition très simple, qui ne requiert aucune compétence informatique particulière. Le protocole est destiné à vous guider dans votre écriture.

#### Comment soumettre des articles à Sens public ?

Les articles pour Sens public doivent être rédigés avec l'éditeur de texte [Stylo](https://stylo.ecrituresnumeriques.ca/) puis partagés avec l'adresse de la rédaction (proposition@sens-public.org) directement depuis la plateforme Stylo. Un courriel doit ensuite être envoyé à l'adresse proposition@sens-public.org pour signaler la soumission.


[*La documentation complète sur l'usage de Stylo est disponible ici*](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)


#### Comment créer un article sur Stylo ?

Vous devez d'abord vous [créer un compte](https://front.stylo.ecrituresnumeriques.ca/register) sur la plateforme.

Vous aurez ensuite accès à l'interface, qui se présente comme suit : 

![Stylo - page d'accueil](https://i.imgur.com/IIQBlkJ.png)

Par défaut, un premier article "How to Stylo" est présent sur la plateforme, et détaille chaque étape de la rédaction d'un article. N'hésitez pas à vous y référer si vous souhaitez directement écrire votre article dans Stylo, ou si vous avez des questions sur des articles à traitement particulier (illustrations, etc.).  

Cliquez sur le bouton "Create a new article" (vous devrez renseigner le titre de l'article dans la case prévue à cet effet, puis cliquer à nouveau sur le bouton "create").  

![Edit](https://i.imgur.com/Tr0uctT.png)

L'article apparait désormais dans votre liste d'articles. Cliquez sur "Edit" pour accéder à l'environnement d'édition, qui se présente ainsi : 

![Environnement d'édition Stylo](https://i.imgur.com/Ba4GYMD.png)

L'environnement d'édition est composé de 5 modules :

- au centre : l'espace d'écriture, consacré au corps de texte de l'article
- à droite : le bouton [Metadata] ouvre l'éditeur de métadonnées
- à gauche :
    - les Versions pour naviguer et agir sur les différentes versions de l'article
    - le Sommaire de l'article liste les titres de niveau 2 et 3
    - la Bibliographie liste les références bibliographiques
    - les Statistiques offrent quelques données quantitatives sur l'article


#### Comment rédiger votre article ?

Pour déposer votre article, il vous suffit de suivre les quatre étapes suivantes :  

##### 1. Renseigner les métadonnées de l'article

Ouvrez l'éditeur de métadonnées en cliquant sur le bouton "Metadata" qui se trouve sur votre droite.

![Métadonnées Stylo](https://i.imgur.com/HrQIV1u.png)


Les métadonnées doivent obligatoirement comprendre les éléments suivants :  

- Titre (et sous-tire, le cas échéant)   
- Un résumé en *deux langues*   
- Des mots-clés en *deux langues*   
- Le nom de·s auteur·e·s ainsi que leur identifiant ORCID (voir [ici](https://orcid.org/) pour créer son identifiant ORCID)   

##### 2. Rédiger le corps de l'article

L'article doit être rédigé en markdown (un langage de balisage simplifié, très facile à assimiler et à utiliser). Pour apprendre en 3 minutes comment écrire en Markdown, vous pouvez consulter [cette page](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/syntaxemarkdown.md).

Si vous avez travaillé avec un logiciel de type word, Stylo s'est dôté d'un convertisseur markdown que vous trouverez [ici](https://stylo-export.ecrituresnumeriques.ca/importdocx.html). Une fois votre article converti en format markdown, il vous suffira de le coller dans la partie centrale de l'éditeur.
![Import](https://i.imgur.com/XtZdFrQ.png)

##### 3. Structurer les références

Tous les articles doivent être accompagnés d'une bibliographie structurée, en format bibtex. Vous pouvez directement structurer vos références en bibtex, ou exporter vos références en bibtex grâce à votre outils de gestion de bibliographie, tels que Zotero ou Mendeley (voir le tutoriel de Zotero [ici](http://sens-public.org/IMG/pdf/Utiliser_Zotero.pdf) et celui de Mendeley [là](https://libguides.usask.ca/c.php?g=218034&p=1446316)).

Pour ajouter une bibliographie, il vous faut cliquer sur le bouton "manage bibliography" qui se trouve dans le volet à gauche.

![Manage bibliography](https://i.imgur.com/NBEhlGa.png)

Une nouvelle fenêtre s'ouvrira alors, dans laquelle vous pourrez soit :

- copier-coller l'url de votre bibliothèque Zotero   
- copier-coller le fichier bibtex "brut" dans la case prévue à cet effet   

![Fenêtre bibliographie](https://i.imgur.com/OoW9DmG.png)

Si, en cours d'écriture, vous souhaitez ajouter de nouvelles références, vous pouvez simplement les intégrer via la case "citation"

Les références doivent être reliées dans le corps du texte comme [expliqué ici](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/bibliographie.md). Pour ajouter une référence à l'article, il suffit de cliquer sur la référence, puis de coller la référence dans le texte à l'endroit souhaité. Ainsi, un clic revient à "copier" la clé de la référence dans le presse-papier. Il ne reste plus qu'à la coller dans le corps de texte. ![biblioex](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/pages/uploads/images/biblioex.png)

##### 4. Enregistrer l'article

Stylo sauvegarde automatiquement votre travail.

Cependant, il vous est possible - et fortement conseillé - d'utiliser les fonctions de sauvegarde du logiciel, qui vous permet de nommer les différentes versions de votre article.

![Save](https://i.imgur.com/DYB3OL4.png)

Ainsi, lorsque vous êtes parvenu·e·s à une version que vous jugez satisfaisante, vous pouvez par exemple inscrire, dans la case "Label of the version", la mention "soumission Sens public", puis cliquer sur le bouton "Save major".

**À tout moment, vous pouvez visualiser votre travail en cliquant sur le bouton preview.**

La prévisualisation est accompagnée d'un logiciel d'annotation, qui pourra servir à l'évaluation de votre article.

#### Comment soumettre votre article ?

Lorsque vous êtes parvenu·e·s à une version finale de votre article, il vous suffit simplement le partager avec la rédaction. Vous devez :

- Retourner sur la page Article de Stylo (en cliquant sur le bouton "My articles" en haut de la page),
- Cliquer sur le signe "+" à gauche du titre de l'article
- Cliquer sur le bouton "Share"
- Dans la fenêtre qui apparaît, renseigner l'adresse de la rédaction (proposition@sens-public.org), puis cliquez sur "add", et enfin "Share".
- Un courriel doit ensuite être envoyé à l'adresse proposition@sens-public.org pour signaler la soumission.

![Share](https://i.imgur.com/O4GP1uh.png)

### Protocole de soumission de dossier 
Pour proposer un dossier à la revue, veuillez suivre les instructions relatives ci-dessus pour soumettre une présentation qui doit contenir : 

- un titre de dossier (provisoire)
- une courte description argumentée (200 mots)
- le·s nom·s des directeur·rice·s 
- la liste préliminaire des articles composant le dossier ainsi que leurs auteur·e·s
- une courte bibliographie éditée dans le volet Bibliographie

Veuillez ensuite partager la présentation du dossier avec la rédaction (proposition@sens-public.org).

