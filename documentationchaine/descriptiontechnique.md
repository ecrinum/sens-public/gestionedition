# Description technique du flux éditorial de SP

Le flux est décrit dans l'image:

![](https://raw.githubusercontent.com/EcrituresNumeriques/chaineEditorialeSP/master/assets/chaineSP.png)

Il reste à intégrer dans l'image Stylo.


## Description principales étapes

- soumission sur Stylo (auteur.e)
- évaluation interne
- création identifiant
- évaluation externe et annotation
- révision sur Stylo
  - ajout mots-clés contrôlés
  - catégories articles
  - structure métadonnées
- export: html, xml, pdf -> https://gitlab.huma-num.fr/ecrinum/stylo/stylo-export/ 
- ajout sur git
- publication


## Structure données

Les données de SP sont dans les HTML.

Pour comprendre leur structure, cf le template d'export ici: https://gitlab.huma-num.fr/ecrinum/stylo/stylo-export/-/blob/main/templates/sens-public/templateHtml5.html5

Regarder aussi le yaml par défaut de Stylo.

D'autres données:

- id de l'article, ajouté via idsp.csv . Fonctionnement documenté dans le README.md. **Attention**: l'export stylo parse le xml généré à partir du csv pour attribuer les identifiants xml aux articles! Cf. https://gitlab.huma-num.fr/ecrinum/stylo/stylo-export/-/blame/main/templates/sens-public/template-erudit.xsl#L147 **et** ordsec ici https://gitlab.huma-num.fr/ecrinum/stylo/stylo-export/-/blame/main/templates/sens-public/template-erudit.xsl#L154
- mots-clés contrôlés. Le django a créé la liste à partir de https://framagit.org/marviro/spkeywords. Actuellement Stylo parse juste toute l'api isidore... il serait utile de fournir la liste des mots-clés existants via api à Stylo
- le logo article est ajouté manuellement dans le dossier media dans le repos git avec la nomenclature  `arton+id_sans_sp.ext` par exemple `arton1509.jpg`
- infos carousel: ajoutées sur l'admin django
- **le zotero de SP!** - il y a un gros travail à faire sur cela


L'ensemble des points importants sont résumés dans protocolechaine.md.

## Ensemble de ressources

[ici](../ressources.md)

## Pour l'histoire

Jusqu'à 2020, le site de Sens public était un Spip.

Jusqu'à 2016 et à l'article 1192 les articles ont été édités avec Libreoffice et ensuite importés sur Spip avec un plugin bidouillé par Marcello. Ensuite il y avait une révision finale sur Spip et des métadonnées ajoutées sur Spip.


À partir de 2016, la nouvelle chaîne a été mise en place (cf [ici](https://github.com/EcrituresNumeriques/chaineEditorialeSP/blob/master/ProtocoleSensPublic.md)). C'est la chaîne qui, à partir de 2018, a été implementée dans Stylo.

Les articles anterieurs à 2016 ont été transformé de spip vers un xml érudit et ensuite vers l'html SP (avec des xslt).
Pour la conversion spip2erudit cf : 
https://github.com/EcrituresNumeriques/spip2erudit

Pour la conversion xmlerudit2htmlsp cf https://github.com/EcrituresNumeriques/chaineEditorialeSP/blob/master/xslt/Erudit2HTML.xsl

Mais il y a eu pas mal de bidouilles aussi.

Les articles avant 2016 se trouvent dans le repos SP-archives.

À partir de 2016 les articles se trouvent dans SP-articles.

À partir du 14 février 2024 le repos est unifié et se trouve ici: https://gitlab.huma-num.fr/ecrinum/sens-public/sp-all


## Se rappeler...

Actuellement le parseur django jette à la poubelle des infos importantes contenues dans l'HTML

Question traductions, par exemple - les données ne sont pas dans le django. Certains ne sont pas dans l'html. Il y a un tableau avec les traductions, (dans bidouillesSP ordi Marcello)

[Notes todo django - jamais fait!](https://demo.hedgedoc.org/E04IOSFpQ5SNyGp4JOVkYg#)

[D'autres notes avec problèmes du site actuel - partiellement identique au précédent](https://demo.hedgedoc.org/4Iqa4bq_SfiJ2HF0y-nP2Q#)


## Pour le développement du site: nouvelles fonctionnalités

https://demo.hedgedoc.org/X3jrMfb3SveRDC25qOO5dg#

https://demo.hedgedoc.org/4Iqa4bq_SfiJ2HF0y-nP2Q#


- api
- identifiants personnes: connecter orcid et hypotesis et id site sp (auteur et lecteur)
- biblio et enrichissements? (au moins récupérer chaque réf structurée)
- conversation - récupérer docs nico
- https://framagit.org/ecrinum/sp_hub/-/wikis/Sc%C3%A9nario-Utilisateur

## Documents potentiellement intéressants

https://demo.hedgedoc.org/1O9SD9xKR129geKJ0wUHOg#

https://demo.hedgedoc.org/kWisqtjlQROuBn3m9jv0Wg#

https://demo.hedgedoc.org/4Iqa4bq_SfiJ2HF0y-nP2Q#

éditorial: https://demo.hedgedoc.org/hSFYpTa9Rv2DVMunqdY5Iw#

https://ecrinum.gitpages.huma-num.fr/sens-public/sp-stylo-doc/
