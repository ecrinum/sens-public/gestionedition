# Protocole de publication de Sens public

Ce document décrit l'ensemble de la chaîne éditoriale de Sens public à partir de la soumission de l'article jusqu'à sa publication sur Sens public et au dépôt des XML chez Érudit, en fournissant les liens vers la documentation nécessaire.

## Soumission article

Les articles pour Sens public doivent être rédigés avec l'éditeur de texte [Stylo](https://stylo.ecrituresnumeriques.ca/) puis partagés avec l'adresse de la rédaction (proposition@sens-public.org) directement depuis la plateforme Stylo.
[*La documentation complète sur l'usage de Stylo est disponible ici*](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)

Les consignes sont disponibles sur le site de Sens public ici et sur le document [publier](publier.md) - SVP, en cas de changements, tenez à jour ce document.

Le coordinateur doit contrôler une fois par semaine les articles parvenus sur le compte stylo proposition.

– Le coordinateur ajoute l'article à la liste des articles en évaluation sur Meistertask
– Le coordinateur écrit à l'auteur pour notifier la réception (écrire une réponse
type)
– Le coordinateur envoie l'article à deux évaluateurs en interne en demandant un avis rapide: l'article doit etre évalué ou pas. Si oui, par qui?

Si l'avis est positif, on procède à l'évaluation ouverte.

## Soumission dossier

Une proposition de dossier est d'abord envoyée à la rédaction. Cette proposition doit contenir:

- un titre de dossier (provisoire)
- une courte description argumentée (200 mots)
- le·s nom·s des directeur·rice·s 
- la liste préliminaire des articles composant le dossier ainsi que leurs auteur·e·s
- une courte bibliographie éditée dans le volet Bibliographie

Si le dossier est accepté ensuite les responsables de dossiers doivent fournir tous les articles ensemble sur Stylo

## Évaluation ouverte

L'article est envoyé à deux ou trois évaluateurs qui annotent et commentent sur hypothes.is

- Le coordinateur écrit aux évaluateurs
- Le coordinateurs signale sur Meistertask qu'il a envoyé les demandes d'évaluation (et la date)
- Le coordinateur signale si les demandes ont été acceptées

Une fois reçues au moins deux évaluations:

- Le coordinateur se concerte avec la rédaction
- Il rédige un avis synthétique sur hypothes.is et envoie les liens à l'auteur <!-- détails sur groupes hypothesi.is etc-->
- Il renseigne sur Meistertask l'état de l'article (évaluations reçues, article renvoyé à l'auteur)

Dans le cas d'évaluations positives, on demande à l'auteur les révisions nécessaires. Une fois les révision effectué on passe à la phase d'édition

## Édition

1. Le coordinateur crée un identifiant pour l'article dans [../idsp.csv]. Le csv doit contenir:
```
id,title,authors
```
2. Le coordinateur change le nom de l'article sur Stylo en l'appelant ED+id de l'article
3. Le coordinateur envoie l'article en révision linguistique
4. Le coordinater renseigne l'état de l'article sur Meistertask

Les révisions sont faites sur Stylo. Hypothes.is peut être utilisé <!-- plus de détails-->

L'article est renvoyé à l'auteur pour acceptation révisions

On passe ensuite à la phase publication

## Publication

L'article est renommé CQ+id

Cette phase est réalisé seulement par le coordinateur ou par un membre de la rédaction accrédité

### Checklist

* [ ] renseigner la date de publication dans les métadonnées Stylo 
* [ ] renseigner l'id **avec** SP (majuscule) (ex SP1566) 
* [ ] Vérifier les mots-clés auteur et éditeur en les confrontant avec les mots-clés existants sur le site sens-public.org. Éventuellement éditer le raw 
* [ ] Vérifier que l'url de l'article soit renseigné en entiers (ex: /articles/1509)
* [ ] S'il y a une légende pour le logo, ajouter à la fin du yaml raw: `logocredits: "Texte de la légende"`  
* [ ] Vérifier l'orthographe de la revue dans les métadonnées : Sens public 
* [ ] Vérifier que le directeur de la revue soit le bon : Gérard Wormser 0000-0002-6651-1650
* [ ] Vérifier qu'il y ait un espace avant chaque mot-clé auteur à partir du deuxième (et ensuite vérifier dans le pdf) 
* [ ] Vérifier ISSN 2104-3272
* [ ] Vérifier que, le cas échéant, les informations de traductions soient renseignées
* [ ] Vérifier que, le cas échéant, les informations sur le dossier soient bien renseignées (titre exact) et identifiant "SP+id" et le directeur 
* [ ] créer une version majeure "Version export 1" 
* [ ] renommer l'article "SP*identifiant*"
* [ ] copier la clef de version à partir de l'URL de la version "Version export 1" 
* [ ] coller la clef de version dans l'onglet *Version* du [Stylo export for Sens public](https://stylo-export.ecrituresnumeriques.ca/exportsenspublic.html)
* [ ] renseigner l'*Identifiant* avec "SP*identifiant*"
* [ ] vérfier que le processor est bien renseigné en mode "xelatex"
* [ ] exporter 
* [ ] contrôler les erreurs affichées en rouge
* [ ] contrôler le PDF généré en ligne (auteur, métadonnées, date, notes, bibliographie) 
* [ ] télécharger le dossier .zip 
* [ ] ajouter le dossier SP+id dans SP-articles/année_de_publication/
- [ ] ajouter dans le dossier media le logo article avec la nomenclature  `arton+id_sans_sp.ext` par exemple `arton1509.jpg`

Maintenant on peut git add et ensuite `git commit -m "SP+id publication + clé_version_stylo"` et git push

Ajouter bibtex au zotero <!--margot explique comment-->

### Mise en ligne


Se connecter en ssh à ecrituresnumeriques

```
cd /docker/sphub/articles
bash update_articles.sh .
cd docker/sphub/src/docker/
docker-compose -f docker-compose.prod.yml exec app python manage.py importhtml id_sans_sp
```

Ensuite aller sur sens-public.org/admin.

1. Dans la page articles sélectionner l'article et cliquer sur "publier"
2. Vérifier dans le front le nom de l'auteur (s'il a d'autres articles, qu'il n'y ait pas un doublon)
3. Vérifier qu'il n'y ait pas de doublons de mots-clés
4. Vérifier qu'il n'y ait pas de doublons de dossier, s'il s'agit d'un dossier
5. Vérifier que le logo s'affiche et que la légende (le cas échéant) s'affiche
6. Vérifier que les traducteurs s'affichent
7. Vérifier que le pdf s'affiche
8. Vérifier les informations de citation
9. Vérifier que les références et les notes soient cliquables dans le corps du texte
10. Vérifier qu'il n'y ait pas de formatage étrange dans la bibliographie (s'il y a du gras il est possible qu'un exposant dans le bibtex ait créé dans l'html un `<em></em>` vide, ce qui fait bugguer le parseur html).

L'article peut ensuite être ajouté à la une dans la page admin carousel_sp

##  Post-publication
* [ ] dans Stylo, renommer l'article en "PSP*identifiant spip*
Il serait bien d'ajoter des recommandation sur la communication
