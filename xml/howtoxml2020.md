# Protocole transormation XML 2020

Le fichier sommaire.xml qui permet de rajouter de rajouter les informations orseq et idref est désormais maintenu à la racine de ce dépôt en ligne. IL est appelé par le fichier de transformation xslt XHTML52erudit.xsl qui se trouve dans templates-sp et qui est donc appliqué automatiquement par stylo-export. 

Le script makesommaire.sh servait auparavant pour faire le csv, il n'est plus nécessaire désormais. 

Pour la production d'un xml pour un nouvel article : exemple 1521 

AVANT l'export sur stylo-export : 

1. Mettre à jour idsp.csv
- cd gestionedition/xml/scripts
- gedit makeSommaireNew.xq 
(à la ligne 13 : changer le chemin pour renseigner le chemin de idsp.csv de votre machine)
- basex makeSommaireNew.xq > ../../sommaire.xml
- mv sommaire.xml /home/mame/CRCEN/gitlab/gestionedition/

2. article exporté :
- cd gestionedition/xml/scripts 
- bash makexml.sh /home/mame/CRCEN/gitlab/SP-articles/Edition2020/SP1521/SP1521 /home/mame/CRCEN/gitlab/templates-sp/


Vérifier que l'ordseq apparaisse dans le fichier .erudit.xml des articles : si il n'apparait pas, vérifier que l'url dans le fichier html soit bien renseigné comme suit "/articles/*id sans SP*".

Les articles 1342, 1378, 1413, 1438, 1463, 1465, 1466, 1474, 1483, 1485, 1508, 1509, 1511, 1513, 1514 dans Edition2020 ont été traités manuellement en local parce que l'html a été modifié après leur édition en stylo. Pour ces articles et pour les futurs articles SP, suivre les instructions suivantes à partir du point 5. Gestion des images. 

-----

Dans tout le protocole suivant, on se place dans le dossier Edition2020 : `cd Edition2020`

## 1. Préparation sommaire

1. générer un csv avec **makesommaire.sh**
2. nettoyer manuellement sommaire2020.csv et le rendre conforme
   1. vérifier notamment que tout ce qui est dans Edition2020 a bien été publié, sinon déplacer l'article dans Edition2021 : `git mv SP1444 ../Edition2021/`
   1. changer les titres de dossier en id de dossier
   2. ajouter les ordseq selon les règles en vigueur
3. générer sommaire2020.xml à partir de sommaire2020.csv (voir le script XQUERY makesommaire.xq)

sommaire2020.xml sera ensuite utilisé dans la transfo XSL (html>xmlErudit) pour renseigner certaines données (ordseq, thref).

## 2. Vérifier que tous les articles ont un XML

`tree -L 2 -D -P '*.xml|*.html'`


## 3. Vérification de la conformité des données dans la chaine yaml/md>html>xml

Le script **checkxml.sh** génère un CSV (checkxml.csv) permettant de visualiser rapidement les incohérences entre les dates de modification (dates de commit) des fichiers yaml, md, html et xml. On considèrera que le xml doit être postérieur au html, lui-même postérieur au md OU au yaml.

À partir du tableau checkxml.csv, il faut vérifier les dates de production de chacun des fichiers, et en fonction relancer les conversions (soit xml, soit html+xml). Un autre point de référence consiste également à vérifier si le XML a été produit *après* la dernière version de la feuille de style XSL (templates-stylo/XHTML52erudit.xsl). On considère en effet que la feuille de style peut-être corrigée ou améliorée durant l'année, nécessitant alors de refaire les conversions pour les XML produit avant la modification de la XSL.

## 4. Passage en revue du checkxml.csv

Selon la comparaison des dates entre yaml/html/xml, je relance le makearticle.sh (voir [À savoir](#a-savoir)).

## 5. Gestion des images

Vérifier que les images sont correctement nommées (cf nomenclature Erudit) et correspondent aux uri des images dans le XML. Normalement, tout est propre de ce côté là avec la chaîne Stylo Export SP.

## 6. Livraison à Erudit

Pour un numéro donné (par exemple `2019`), Erudit demande la structure et la nomenclature de fichiers suivantes :

```bash
~/gitlab/senspublic/data/2019/
├── 1328-article.pdf
├── 1328-article.xml
├── 1336-article.pdf
├── 1336-article.xml
├── 1339-article.pdf
├── 1339-article.xml
├── ...
├── 1462-article.pdf
├── 1462-article.xml
└── images
    └── normales
        ├── SP1336-img1.jpg
        ├── SP1336-img2.png
        ├── SP1336-img3.jpg
        ├── SP1343-img1.png
        ├── SP1362-img1.png
        ├── SP1362-img2.png
        ├── SP1363-img1.jpg
        ├── SP1363-img2.png
```

Soit pour livrer, il faut
1. renommer les xml
2. renommer les pdf
3. créer un répertoire et sous répertoire `images/normales/` et y copier toutes les images de l'année. Attention, il faut veiller à retirer les images logo ou tout autre fichier présent dans les dossiers `media/`
4. insérer le fichier sommaire2020.xml
5. compresser et envoyer à Erudit, ou bien protocole git sur le repo `git@gitlab.erudit.team:EcrituresNumeriques/senspublic.git`


# Erreurs et cas particuliers

## Erreurs classiques notifiées lors de la validation contre schéma

Pour les articles 2019 :

- [x] SP1343 - corrections diverses yaml, md (image+dedicace) > création d'un erudit.md et erudit.html
- [x] SP1380 - warning pour blockquote>li>p mais c'est bon
- [x] SP1383 - mise à jour yaml, md, html + réédition md >html > xml pour se conformer au schéma Erudit
- [x] SP1420 - erreur images sans légende
- [x] SP1462 - source ligne 198
- [x] SP1409 - pb d'alinea
- [x] SP1393 - source xml-ligne 242
- [x] SP1380 - elemlist xml-ligne 183
- [x] SP1374 - yaml issnum
- [x] SP1367 - image ligne 318
- [-] SP1369 - liensimple ligne 601 >> erreur inexpliqué... :/
- [x] SP1379 - traitement des divs >> tableau
- [x] SP1395 - mise à jour yaml, mais besoin de retravailler pour un tableau

## Cas particuliers pour certains articles

Il est possible qu'il faille produire un markdown dédié pour la production du XML Erudit. Par exemple certaines balises md/html ne sont pas pris en compte par la XSL.

Exemple de traitement pour les 3 articles de Peter Brook:

  1. dupliquer le md `cp SP1370.md SP1370.erudit.md`
  2. éditer le md dédié (*.erudit.md) pour élaguer toutes les particularités de l'article, ou les adapter pour Erudit.
  3. lancer la commande de transformation de md > html

    `pandoc --verbose --standalone --section-divs --ascii --toc --template=/home/nicolas/gitlab/templates-sp/templateHtml5.html5 --csl=/home/nicolas/gitlab/templates-sp/chicagomodified.csl -f markdown -t html5 --filter pandoc-citeproc SP1370.erudit.md SP1370.yaml -o SP1370.erudit.html`

4. lancer la commande de transfo html > xml

    `saxon-xslt-html -s:SP1370.erudit.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:SP1370.erudit.xml`
    `xmllint --schema /home/nicolas/gitlab/templates-sp/schema/eruditarticleNoAnnotations.xsd SP1370.erudit.xml --noout`

## Traitement des tableaux

Les tableaux ne sont pas traités actuellement par le script **XHTML52erudit.xsl**. On doit alors traiter le xml produit avec les deux scripts suivants

1. faire tourner le script templates-sp/scripts/**tabletotabtexte.xsl**

    saxon-xslt-html -s:SP1379/SP1379.erudit.xml -xsl:/home/nicolas/gitlab/templates-sp/scripts/table2tabtexte.xsl -o:SP1379/SP1379.erudit.tableau.xml

Todo: ce bout de xsl peut/doit être réintégré dans le script XHTML52erudit.xsl.

2. faire tourner le script templates-sp/scripts/**idifiers.sh**

    bash ~/gitlab/templates-sp/scripts/idifier.sh SP1379/SP1379.erudit.tableau.xml

Ce script ajoute à chacune des balises du tableau les identifiants de ligne, de colonne et de cellule.

3. à la main: nettoyage du xml, ajout des légendes (non traitées par les scripts), éventuellement une balise numéro `<no>`


## Article de type _Lecture_

Pour ces article, il faut ajouter manuellement une balise `<trefbiblio>` dans le `<grtitre>` :

1. ajouter une balise `<trefbiblio>` donnant la référence bibliographique de l'ouvrage analysé.
2. éventuellement supprimer la balise `<sstitre>` s'il est en doublon avec `<trefbiblio>` (souvent le cas).
3. supprimer la balise `<refbiblio>` correspondant au `<trefbiblio>`. S'il n'y avait en bibliographie que la référence analysée, alors supprimer tout `<grbiblio>`. Si par ailleurs, il n'y a pas de notes de bas de page, supprimer aussi `<partiesann>`.


## Retour Erudit Validation

## Retour Erudit Edition

Ces informations sont utiles pour dresser une check-liste d'erreure communes. J'ai parfois amélioré le script pour traiter les problèmes identifiés par Erudit.

1.    Certains marquage semblent avoir été oubliés lors de la conversion en EruditArticle.

     - [x]   Je l’ai remarqué pour l'article " Traversé de fantômes ", où certains passages sont en italique sur le site de Sens Public, mais pas sur Érudit. Par exemple, les mots  "Y penser sans cesse" dans le résumé
     - [x]   correction de la XSLT en conséquence : le titre sera désormais renseigné par le titre formatté.

2.    Pour certains articles, le contenu est balisé en paragraphe, alors qu’un balisage fin serait plus approprié :

     - [x]   Élément “epigraphe”  : dans l'article "Donald Trump et l’Agneau de Scythie", le contenu du premier paragraphe devrait être balisé avec l'élément "epigraphe"
     - [x]   Élément “merci” : dans l'article "Des outils à la trace", le contenu des paragraphes 66 à 68 devrait être balisé avec l'élément "merci"
     - [x]   Élément “notegen” : Dans l'article "Des outils à la trace", le contenu du paragraphe 69 devrait être balisé avec l'élément "notegen@typenoteg="edito""

3.    Étrangement, certains nom et prénom sont inversés, alors qu’ils ne le sont pas sur le site de Sens Public.

Pb dans les yaml.

      - [x]  Articles "Presence and Creation" / "Présence et création" / "Presencia y Creación"
      - [x]  Article "De la disparition scénique entre le postmoderne et le posthumain"
      - [x]  Article "L’authenticité du flou"

4.    Quelques problèmes avec tous les articles de type “compte-rendu” (sont dans les sections “lecture”)

     - [x]   Il faut privilégier l’utilisation de l’élément “trefbiblio” et éviter de répéter  l’information dans les champs “titre” ou “sstitre”, comme c’est le cas actuellement
     - [x]   Le titre de l'ouvrage recensé ne doit pas être balisé dans une bibliographie, comme c’est le cas actuellement

5.    Mauvaise licence utilisée

    - [x]    L’article “Des outils à la trace” est publié sous une licence différente. SVP, mettre les bonnes informations dans le fichier XML de cet article :  
             lien vers la licence : https://creativecommons.org/publicdomain/zero/1.0/deed.fr
             icône : icône : https://licensebuttons.net/l/zero/1.0/88x31.png

6.    Assurez-vous que chaque mot-clé doit être séparé et balisé dans un élément "motcle" distinct

       - [x] Ce n'est pas le cas pour les articles de l'article "The Disappearence of Presence"

C'est une erreur provenant des yaml/Stylo. Corrections faites, sur les yamls et html.
Les xml ont été corrigés manuellement.

7.    Plusieurs articles contiennent encore des balises HTML :

    - [x]    <em> &lt;em&gt;
         ./1409-article.xml
         ./1362-article.xml
         ./1377-article.xml
         ./1436-article.xml
         ./1395-article.xml

    - [x]    <sup> &lt;sup&gt;
         ./1429-article.xml
         ./1409-article.xml
         ./1397-article.xml
         ./1425-article.xml
         ./1422-article.xml
         ./1431-article.xml >> traiter dans le titre aussi
         ./1426-article.xml
         ./1416-article.xml

    - [x]    <p> &lt;p&gt;
         ./1409-article.xml
         ./1397-article.xml

   - [x]     <i> &lt;i&gt;
         ./1370-article.xml
         ./1371-article.xml
         ./1372-article.xml

Ces erreurs proviennent des résumés dans lesquels subsistent des balises de formattage HTML qui ne sont pas traités. J'ai effectué une correction de la XSLT qui prendra désormais en compte les résumés formattés.

8.    J’ai trouvé les coquilles suivantes :

- [x] Article "Conversations autour de (l’anti) roman latino-américain à l’Université du Chili" : une entitée HTML mal balisée n'a pas pu être transformée ("&ampnsbp")
- [x] Article "Conversations autour de (l’anti) roman latino-américain à l’Université du Chili" : contient du markdown mal formé (3 - Disjecta Membra. ](http://sens-public.org/article1423.html))


## A savoir

Voici les commandes utilisées pour 1) générer le xml à partir du html, 2) valider le xml produit contre schéma.

Exemple pour SP1377:

1. `saxon-xslt-html -s:SP1377.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:SP1377.erudit.xml`
2. `xmllint --schema /home/nicolas/gitlab/templates-sp/schema/eruditarticleNoAnnotations.xsd SP1377.erudit.xml --noout`

**saxon-xslt-html** est le script suivant :

```bash
#!/bin/sh

exec java -cp /usr/share/maven-repo/net/sf/saxon/Saxon-HE/9.8.0.8/Saxon-HE-9.8.0.8.jar:/usr/bin/tagsoup-1.2.1.jar net.sf.saxon.
Transform -x:org.ccil.cowan.tagsoup.Parser  "$@" \!indent=yes
```

Le script **makearticle.sh** (usage: `./makearticle SP1234`) :

1. génère html à partir du md/yaml/bib,
2. puis le xml à partir du html,
3. et valide le xml contre schéma
