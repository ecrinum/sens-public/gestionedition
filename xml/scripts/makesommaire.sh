#!/bin/sh

rm sommaire.csv
echo "phase;numero;dossierMulti;dossier;ordseq;rubrique;rubrique4Erudit;statut;id;titre;datePub;dateModif;auteurs;keywords;directeur;statut;phase" > sommaire.csv

for var in $(ls -d SP*); do
cd $var
year=$(yq r $var.yaml year)
title=$(yq r $var.yaml title)
authorname=$(yq r $var.yaml authors[0].surname)
type=$(yq r $var.yaml typeArticle[0])
id=$(yq r $var.yaml id)
dossier=$(yq r $var.yaml dossier[0].title)
date=$(yq r $var.yaml date)

case $type in
  Essai ) type=58 ;;
  Lecture ) type=76 ;;
  "Sommaire dossier" ) type=58 ;;
  Chronique ) type=114 ;;
  Création ) type=60 ;;
  Entretien ) type=113 ;;
esac

echo "$id : $type"
echo ";$year;;$dossier;;$type;;;$id;$title;$date;;$authorname;;;;" >> ../sommaire.csv
cd ..
done


