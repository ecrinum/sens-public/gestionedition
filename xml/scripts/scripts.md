# Sommaire scripts

Tous les scripts fonctionnent s'ils sont placés dans le répertoire de l'année en cours, par exemple dans `Edition2020/`

1. checkxml.sh : Génère un tableau permettant de repérer les incohérences de dates de création/modification des fichiers yaml/md/html/xml en se basant sur les git-log de SP-articles
2. makearticle.sh : Génère successivement le html puis le xml à partir des sources yaml/md/bib d'un article
   - `./makearticle.sh SP1234`
3. makesommaire.sh : Génère un fichier `sommaire.csv` à partir des sources yaml de chaque article.
4. makeSommaire.xq : Génère un fichier `sommaire.xml` à partir du fichier `sommaire.csv`
5. makexml.sh : Génère le xml d'un article à partir de la source html d'un article
