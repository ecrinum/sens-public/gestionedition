#!/bin/sh


echo "saxon $1.html to $1.erudit.xml"
saxon-xslt-html -s:$1/$1.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:$1/$1.erudit.xml

echo "validation schema $1.erudit.xml"
xmllint --schema /home/nicolas/gitlab/templates-sp/schema/eruditarticleNoAnnotations.xsd $1/$1.erudit.xml --noout

git diff --color-words $1/$1.erudit.xml
