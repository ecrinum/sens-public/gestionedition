#!/bin/sh

rm checkxml.csv
echo "id;format;lastlog;datelog" > checkxml.csv

for var in $(ls -d SP*); do
cd $var
id=$(yq r $var.yaml id)
echo "$id : yaml"
lastlog=$(git log --oneline --pretty=oneline $var.yaml | head -n 1)
datelog=$(git log --pretty=%ci $var.yaml | head -n 1)
echo "$id;yaml;$datelog;$lastlog" >> ../checkxml.csv
echo "$id : html"
lastlog=$(git log --oneline --pretty=oneline $var.html | head -n 1)
datelog=$(git log --pretty=%ci $var.html | head -n 1)
echo "$id;html;$datelog;$lastlog" >> ../checkxml.csv
echo "$id : xml"
lastlog=$(git log --oneline --pretty=oneline $var.erudit.xml | head -n 1)
datelog=$(git log --pretty=%ci $var.erudit.xml | head -n 1)
echo "$id;xml;$datelog;$lastlog" >> ../checkxml.csv
cd ..
done
