#!/bin/bash
for filename in $1/*/*.html; do

echo $filename
echo $(basename "$filename" .html)
echo $1/$(basename "$filename" .html)/$(basename "$filename" .html).erudit.xml

#commande qui correspond à l'installation d'Antoine : https://ecrinum.gitpages.huma-num.fr/stylo/xml-documentation/pages/scripts/#saxon
saxon-xslt-html -s:$filename -xsl:$2XHTML52erudit.xsl -o:$1/$(basename "$filename" .html)/$(basename "$filename" .html).erudit.xml !indent=yes

done
