xquery version "3.1" ;

(: 
  Ce script produit le sommaire XML (tel qu'Erudit le demande), à partir 
  d'un csv produit avec le script makeSommaire.sh 
  Protocole: 
  1. produire sommaire2020.csv avec makeSommaire.sh
  2. éditer manuellement sommaire2020.csv (ordseq, dossier)
  3. produire sommaire2020.xml avec makeSommaire.xq
:)


declare variable $local:base := "/home/mame/CRCEN/gitlab/SP-articles/Edition2021/" ;


let $options := map { 'separator': 'semicolon', 'header': "true", 'format':'direct' }
let $csv := fn:unparsed-text($local:base || 'sommaire2021.csv')
let $baserefs := csv:parse($csv, $options )
let $annee := '2021'

return <sommaire year="{$annee}">{
for $record in $baserefs//*:record[fn:data(*:numero) = $annee]
     let $id := $record/*:id
     let $ordseq := $record/*:ordseq
     let $titre := $record/*:titre
     let $dossier := $record/*:dossier
     return <article id="{$id}" ordseq="{$ordseq}" dossier="{$dossier}">{$titre}</article>
}
</sommaire>
