xquery version "3.1" ;

(:
  Ce script produit le sommaire XML (tel qu'Erudit le demande), à partir
  d'un csv produit avec le script makeSommaire.sh
  Protocole:
  1. produire sommaire2022.csv avec makeSommaire.sh
  2. éditer manuellement sommaire2022.csv (ordseq, dossier)
  3. produire sommaire2022.xml avec makeSommaire.xq
:)


declare variable $local:base := "/Users/Eugenie/Documents/gestionedition/" ;


let $options := map { 'separator': 'semicolon', 'header': "true", 'format':'direct' }
let $csv := fn:unparsed-text($local:base || 'idsp.csv')
let $baserefs := csv:parse($csv, $options )
let $annee := '2024'
let $numeroid := 'sp02131n2024'

return <sommaire year="{$annee}">{
for $record in $baserefs//*:record[fn:data(*:numero) = $annee]
     let $id := $record/*:id
     let $ordseq := $record/*:ordseq
     let $titre := $record/*:titre
     let $dossier := $record/*:dossier
     return <article id="{$id}" ordseq="{$ordseq}" dossier="{$dossier}" numeroid="{$numeroid}">{$titre}</article>
}
</sommaire>
