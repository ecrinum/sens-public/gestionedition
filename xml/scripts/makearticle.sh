#!/bin/sh

echo "pandoc html $1"
pandoc --verbose --standalone --section-divs --ascii --toc --template=/home/nicolas/gitlab/templates-sp/templateHtml5.html5 --csl=/home/nicolas/gitlab/templates-sp/chicagomodified.csl -f markdown -t html5 --bibliography=$1/$1.bib --filter pandoc-citeproc $1/$1.md $1/$1.yaml -o $1/$1.html

echo "saxon $1.html to $1.erudit.xml"
saxon-xslt-html -s:$1/$1.html -xsl:/home/nicolas/gitlab/templates-sp/XHTML52erudit.xsl -o:$1/$1.erudit.xml

echo "validation schema $1.erudit.xml"
xmllint --schema /home/nicolas/gitlab/templates-sp/schema/eruditarticleNoAnnotations.xsd $1/$1.erudit.xml --noout

git status
