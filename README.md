# Gestion édition Sens public

Dans ce repo il y a :

1. Les fichiers idsp : fichier avec la base des identifiants uniques des articles de *Sens public*
    - le fichier [idsp21](idsp21.csv) concerne l'année 2021
    - le fichier [idsp](idsp.csv) concerne l'année 2020

2. Les fichiers sommaire : sommaire xml des articles de *Sens public* pour la livraison à Érudit
    - le fichier [sommaire2021](sommaire2021.xml) concerne l'année 2021
    - le fichier [sommaire2021](sommaire2020.xml) concerne l'année 2020
    - le fichier [sommaire](sommaire.xml) rassembler les annnées

**Important** : Pour mettre à jour le sommaire, 

```
cd xml/scripts
gedit makeSommaireNew.xq #à la ligne 13 : changer le chemin pour renseigner le chemin de idsp.csv de votre machine
basex makeSommaireNew.xq > ../../sommaire.xml
```

En plus : 

- [La documentation du protocole de publication](documentationchaine/protocolechaine.md)

- [Le protocole pour les auteur.e.s](documentationchaine/publier.md)

- [Liste des ressources pour Sens public](ressources.md) - tous les repos

- [Le script qui remet ensemble le repos archives et le repos articles](update_articles.sh)

- Documentation sur la [Gestion des xml Erudit](xml/howtoxml2020.md)

## Délais

**Évaluation** : 3 semaines. Si accepte mais pas dispo, on va jusqu’à 5 semaines… à voir.
On passe à un autre appel après 10 jours ouvrables.

**Après acceptation**, on relance 1 semaine après la date limite initiale ou si arrête de répondre après question.

**Retour à l’auteur·e** On demande un accusé de réception et que les commaire sont bien accessibles par l’auteur·e après 1 semaine. Après laisse max 4 semaines après retour.

**Retour sur l’édition et BAT**, on laisse une semaine.

